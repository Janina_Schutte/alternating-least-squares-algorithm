# Alternating-Least-Squares-Algorithm for Parameter Reconstruction
This is an implementation of the alternating least squares algorithm for parameter reconstruction. In this file, we firstly summarize the algorithm and secondly introduce the ALS class implemented in this project.

### Alternating Least Squares Algorithm
The presented algorithm uses measurement pairs $`(A_k,f_\theta(A_k))_k`$ to reconstruct a parameter tensor $`\theta`$ for some $`f_\theta : \mathbb{R}^{d\times...\times d} \to \mathbb{R}`$. In every iteration one leg vector of an initial tensor gets updated by solving a linear least squares problem. This way the l2 loss of the tensor decreases on the considered measurement batch in every iteration.  
***Input:***
- initial tensor x_init $`\in\mathbb{R}^{d\times...\times d}`$ of order $`N`$
- measurement tensors $`A_i\in\mathbb{R}^{d\times...\times d},i = 1,...,m`$ of order $`N`$
- corrsponding measures $`b_i = f_\theta( A_i) \in \mathbb{R}`$ for the tensor $`\theta\in\mathbb{R}^{d\times...\times d}`$ of order $`N`$ to be recovered

***Algorithm:***  
>$`x =`$ x_init  
>until all measurements used or convergence critera fulfilled:  
>>for $`i = 1,...,N`$:
>>> take new measurement batch $`(A_k,b_k)_k`$  
>>> conract every $`A_k`$ and $`x`$ except for $`i`$-th leg vector and save in $`\tilde{A}_k`$  
>>> solve least squares minimization and update $`i`$-th leg vector of $`x`$  
>>>```math
>>>x[i] = \min_{\tilde{x}\in\mathbb{R}^d} \sum_{k} \left(b_k- contract(\tilde{A}_k,\tilde{x})\right)^2
>>>```

***Output:*** $`x`$ as an approximation of $`\theta`$

Here $`f_\theta`$ can be chosen arbitrarily. For instance, in tensor regression we have $`f_\theta (A) = \langle A, \theta \rangle`$.

### Implementation

To easily apply this algorithm, we implemented a class ALS, wich can quickly be used for measurement tensors A, measurements b and an initialization x_init by:
> als = ALS(A,b,x_init)  
> als.run_once()

The result can then be accessed via 
> als.x

A more detailed example can be found in the jupyter notebook *Tensor Recovery Tutorial for Uniformly Distributed Measurements*.

The class contains the following attributes:
- A (measurement vectors, size: (m,N,d)) - *has to be passed when creating object*
- b (measurements, size: m) - *has to be passed when creating object*
- x (current solution ,size: (N,d)) - *has to be passed when creating object*
- d (second dimension of tensor to be recovered)
- N (first dimension of tensor to be recovered)
- m (number of available measurements)
- batchsize (number of samples used for the update of one leg vector)
- H (number of iterations over whole tensor)
- l2loss (list of l2 loss of updated tensor on all measurements after each step)
- term (reason for termination)
- step (size of the last step)
- minimal_step_size (The algorithm terminates as soon as the step in one update ist smaller that this value, can be set before running the algorithm, default value is $`0.0001`$)
- minimal_l2_loss (The algorithm erminates as soon as an l2 loss less than this value is reached, can be set before running algorithm, default value is $`0.0005`$)

The main function implemented in our class is  called run_once. It iterates over the data set and updates the leg vectors of the initial tensor in the manner described above. The algorithm terminates, if the step criteria (step size smaller than minimal_step_size) or the l2 loss critera (l2 loss smaller than minimal_l2_loss) is fulfilled, or every data pair has been seen.